#-------------------------------------------------
#
# Project created by QtCreator 2016-04-23T19:50:46
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Programm1
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h \
    circle.h

FORMS    += mainwindow.ui
